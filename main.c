//#############################################################################
//
// FILE:   mcbsp_ex1_loopback.c
//
// TITLE:  McBSP loopback example.
//
//! \addtogroup driver_example_list
//! <h1> McBSP loopback example </h1>
//!
//! This example demonstrates the McBSP operation using internal loopback.
//! This example does not use interrupts. Instead, a polling method is used
//! to check the receive data. The incoming data is checked for accuracy.
//!
//! Three different serial word sizes can be tested.  Before compiling
//! this project, select the serial word size of 8, 16 or 32 by using
//! the \#define statements at the beginning of the code.
//!
//! This program will execute until terminated by the user.
//!
//! \b 8-bit \b word \b example: \n
//!      The sent data looks like this:\n
//!      00 01 02 03 04 05 06 07 .... FE FF\n
//!
//! \b 16-bit \b word \b example: \n
//!      The sent data looks like this:\n
//!      0000 0001 0002 0003 0004 0005 0006 0007 .... FFFE FFFF\n
//!
//! \b 32-bit \b word \b example: \n
//!      The sent data looks like this: \n
//!      FFFF0000 FFFE0001 FFFD0002 .... 0000FFFF \n
//!
//! \b External \b Connections \n
//! - None
//!
//! \b Watch \b Variables: \n
//! - \b txData1 - Sent data word: 8 or 16-bit or low half of 32-bit
//! - \b txData2 - Sent data word: upper half of 32-bit
//! - \b rxData1 - Received data word: 8 or 16-bit or low half of 32-bit
//! - \b rxData2 - Received data word: upper half of 32-bit
//! - \b errCountGlobal   - Error counter
//!
//! \note txData2 and rxData2 are not used for 8-bit or 16-bit word size.
//
//#############################################################################
// $TI Release: F2837xD Support Library v3.11.00.00 $
// $Release Date: Sun Oct  4 15:55:24 IST 2020 $
// $Copyright:
// Copyright (C) 2013-2020 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//   Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the
//   distribution.
//
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//#############################################################################

//
// Included Files
//

#include <stdint.h>

#include "board.h"
#include "device.h"
#include "driverlib.h"
#include "scistdio.h"
#include "bsp.h"
#include "hal_extadc.h"
#include "hal_cla.h"
#include "log.h"
#include "cla_shared.h"
#include "gpio.h"
//
// Defines
//

#define WAITSTEP            asm(" RPT #255 || NOP")
#define NUM_BUFFERS         5

//#define MAX_TRANSFERS   100
#define STRUCTURE_SIZE      EXT_ADC_NCHANNELS * BUFFER_SIZE

//
// Globals
//
//int32_t rx_data_buffer_a[BUFFER_SIZE];
//int32_t rx_data_buffer_b[BUFFER_SIZE];
//uint16_t *ptr_buffer_receive = (uint16_t *) & (rx_data_buffer_a[0]);

//volatile bool dma_finished;
//uint32_t CLA_COUNT = 0;
//uint32_t tx_data;
//volatile bool transfer_finished;


//int rx_transfer_cnt = 0;
//int curr_buf_pos = 0;
//
// Function Prototypes
//
// __interrupt void localDMAEXTADCISR(void);
//__interrupt void localRxEXTADCINTAISR(void);
//__interrupt void localDUMMYEXTADCINTAISR(void);

// void ads1274_printf_channel(uint16_t channel_id);

// #pragma DATA_SECTION(rx_data_buffer_a, "ramgs1")
// #pragma DATA_SECTION(rx_data_buffer_b, "ramgs1")

#pragma DATA_SECTION(cla_sh_tmp,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_val,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_coef,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_cnt_sample,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_ptr_tmp,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_ptr_out,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_temp_mar1,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_free_cnt,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_next_buf_addr,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_eob_addr,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_ds_rate,"CLADataLS0")
#pragma DATA_SECTION(cla_sh_out,"CLADataLS1")

uint16_t        cla_sh_temp_mar1;
data_channel_t  cla_sh_coef[EXT_ADC_NCHANNELS];
data_channel_t  cla_sh_val[EXT_ADC_NCHANNELS];
float32_t       cla_sh_tmp[EXT_ADC_NCHANNELS];
float32_t*      cla_sh_ptr_tmp;
uint16_t        cla_sh_cnt_sample;
data_cla_out_t  cla_sh_out[MAX_BUFFERS];
int32_t*        cla_sh_ptr_out = cla_sh_out[0].buffer[0];
uint32_t        cla_sh_free_cnt = 0;
uint16_t        cla_sh_next_buf_addr;
uint16_t        cla_sh_eob_addr;
uint16_t        cla_sh_ds_rate = 13; ///< initial output frequency of 5KHz.

//
//Task 1 (ASM) Variables
//
data_cla_out_t* next_read_buf = &cla_sh_out[0];
uint16_t        pending_buffers = 0;
uint16_t        next_buffer = 1;
bool            go = false;
bool            ready = false;
//
// Function Prototypes
//

__interrupt void cla1Isr1();
//__interrupt void cla1Isr2();
//__interrupt void cla1Isr3();
//__interrupt void cla1Isr4();
//__interrupt void cla1Isr5();
//__interrupt void cla1Isr6();
//__interrupt void cla1Isr7();
//__interrupt void cla1Isr8();


//
// Main
//
void main(void)
{
//  tx_data = 100;
//  int var;
//  transfer_finished = false;
//  for (var = 0; var < BUFFER_SIZE; ++var) {
//    rx_data_buffer_a[var] = 0x55555555;
////    rx_data_buffer_b[var] = 0;
//  }

  int err;                            ///< Error handler

  //
  // Store buffer setpoints for CLA buffer write.
  //
  cla_sh_next_buf_addr = cla_sh_ptr_out;

  cla_sh_eob_addr = cla_sh_next_buf_addr + WORDS_PER_BUFFER * MAX_BUFFERS;

  cla_sh_next_buf_addr = cla_sh_next_buf_addr + WORDS_PER_BUFFER;


  //
  // Initialize device clock and peripherals.
  //
  Device_init();

  //
  // Disable all the interrupts.
  //
  DINT;

  //
  // Setup GPIO by disabling pin locks and enabling pullups.
  //
  Device_initGPIO();

  //
  // Initialize GPIO.
  //
  Board_init();

  //
  // Initialize PIE and clear PIE registers. Disables CPU interrupts.
  //
  Interrupt_initModule();

  //
  // Initialize the PIE vector table with pointers to the shell Interrupt
  // Service Routines (ISR).
  //
  Interrupt_initVectorTable();
  //
  //Interrupt_register(mcbsp_channels[MCBSP_EXT_ADC].pie_tx_addr, localDUMMYEXTADCINTAISR);

  uint32_t valor = 16;
  SCIprintf("\n\n\n\n\n\n\n\n");
  LOG_INFO("Device started! powering up peripherals %u", valor);
  hal_init_cla();
  hal_cla_config_memory();
  hal_cla_task_config();

  //
  // EXTADC start
  //
  if (err = hal_extadc_init())
  {
    LOG_ERROR("%i during external ADC initialization", err);
    ESTOP0;
  }
  if ( err = hal_extadc_config() )
  {
    LOG_ERROR("ERROR %i while configuring external ADC services", err);
    ESTOP0;
  }
/*
  if (err = hal_extadc_transfer_config(ptr_buffer_receive,
                                       sizeof(int32_t) * BUFFER_SIZE))
  {
    LOG_ERROR("ERROR %i while configuring external adc transfer", err);
    ESTOP0;
  }
*/
  if ( err = hal_extadc_start() )
  {
    LOG_ERROR("ERROR %i while starting external ADC peripherals", err);
    ESTOP0;
  }

  //hal_extadc_dummy_send(0x555555);
  //
  // Enable McBSP Tx and RX interrupts in PIE block.
  //
  //Interrupt_enable(INT_MCBSPB_TX);
  //
  // Enable group 11 CPU interrupt.
  //
  IER = 0x0400;

  //
  // Enable global interrupts.
  //
  EINT;
  ERTM;
  SCIprintf("\n\n   ddata = [");
  while (1)
  {

      while (go)
      {
          hal_extadc_stop();
          int i;
          for (i = 0; i < BUFFER_SIZE; i++)
          {
              SCIprintf("%i,", next_read_buf->buffer[i][0]);
              SCIprintf("%i,", next_read_buf->buffer[i][1]);
              SCIprintf("%i,", next_read_buf->buffer[i][2]);
              SCIprintf("%i;", next_read_buf->buffer[i][3]);
          }

          next_read_buf = &cla_sh_out[next_buffer];
          next_buffer++;
          next_buffer %= 5;
          pending_buffers--;
          if (pending_buffers == 0)
          {
              SCIprintf(" ];");
              while(1)
              {

              }
          }
      }
  }
//      while(CLA_getTaskRunStatus(CLA1_BASE, CLA_TASK_1))
//      {
//      }
//      WAITSTEP;
//    if (transfer_finished == 1)
//
//    {
//        ads1274_printf_channel(0);
//        ads1274_printf_channel(1);
//
//      //END PROGRAM
//      ESTOP0;
//    }
}

//
// cla1Isr1 - CLA1 ISR 1
//
__interrupt void cla1Isr1 ()
{
    //
    // Acknowledge the end-of-task interrupt for task 1
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP11);
    pending_buffers++;
    if (!ready)
    {
        if (pending_buffers == NUM_BUFFERS)
        {
            pending_buffers = 0;
            ready = true;
        }
    }
    else
    {
        if (pending_buffers == NUM_BUFFERS)
        {
            go = true;
        }
    }

    //
    // Uncomment to halt debugger and stop here
    //
    //asm(" ESTOP0");
}

//
// cla1Isr2 - CLA1 ISR 2
//
//__interrupt void cla1Isr2 ()
//{
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP11);
//    asm(" ESTOP0");
//}
//
////
//// cla1Isr3 - CLA1 ISR 3
////
//__interrupt void cla1Isr3 ()
//{
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP11);
//    asm(" ESTOP0");
//}
//
////
//// cla1Isr4 - CLA1 ISR 4
////
//__interrupt void cla1Isr4 ()
//{
//    asm(" ESTOP0");
//}
//
////
//// cla1Isr5 - CLA1 ISR 5
////
//__interrupt void cla1Isr5 ()
//{
//    asm(" ESTOP0");
//}
//
////
//// cla1Isr6 - CLA1 ISR 6
////
//__interrupt void cla1Isr6 ()
//{
//    asm(" ESTOP0");
//}
//
////
//// cla1Isr7 - CLA1 ISR 7
////
//__interrupt void cla1Isr7 ()
//{
//    asm(" ESTOP0");
//}
//
////
//// cla1Isr8 - CLA1 ISR 8
////
//__interrupt void cla1Isr8 ()
//{
//    //
//    // Acknowledge the end-of-task interrupt for task 8
//    //
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP11);
//    //
//    // Uncomment to halt debugger and stop here
//    //
//    asm(" ESTOP0");
//}

/*
>>>>>>> mcbsp_loop_mode
void ads1274_printf_channel(uint16_t channel_id)
{
  ASSERT(channel_id < EXT_ADC_NCHANNELS - 1);
  uint16_t var = 0;

  uint16_t start_index = BUFFER_PER_CHANNEL * channel_id;
  SCIprintf("\n\n\n\nPRINT CHANNEL: %u\n", start_index);
  SCIprintf("BUFFER A: %u\n\n", start_index);
  for (var = 0; var < BUFFER_PER_CHANNEL; ++var) {
    SCIprintf("%i;", rx_data_buffer_a[start_index + var]);
  }
  SCIprintf("\nBUFFER B: %u\n\n", start_index);
  for (var = 0; var < BUFFER_PER_CHANNEL; ++var) {
    SCIprintf("%i;", rx_data_buffer_b[start_index + var]);
  }
}




__interrupt void localDMAEXTADCISR(void)
{
  Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP7);
  rx_transfer_cnt++;
  //
  // Dest start address = buffer & Src start address = MCBSPA DRR
  //
  if (ptr_buffer_receive == (uint16_t *) & (rx_data_buffer_a[0]))
    ptr_buffer_receive = (uint16_t *) & (rx_data_buffer_b[0]);

  else
    ptr_buffer_receive = (uint16_t *) & (rx_data_buffer_a[0]);

//  if (hal_extadc_transfer_config((uint16_t *)ptr_buffer_receive,
//                                 sizeof(*ptr_buffer_receive) * BUFFER_SIZE));
  if (rx_transfer_cnt == MAX_TRANSFERS)
  {
    hal_extadc_stop();
    dma_finished = 1;
  }
  return;
}
*/
//
//__interrupt void localDUMMYEXTADCINTAISR(void)
//{
//
////    hal_extadc_dummy_send( tx_data );
////    tx_data = tx_data + 1;
//
//
//    //
//    // Acknowledge the interrupt to receive more interrupts.
//    //
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP6);
//}
//
//__interrupt void localRxEXTADCINTAISR(void)
//{
//    rx_transfer_cnt++;
//
//    hal_extadc_receive(&rx_data_buffer_a[curr_buf_pos]);
//    curr_buf_pos++;
//    //
//    // Acknowledge the interrupt to receive more interrupts.
//    //
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP6);
//    if ( rx_transfer_cnt == BUFFER_SIZE )
//    {
//        rx_transfer_cnt = 0;
//        curr_buf_pos = 0;
//
//    }
//}
//
// End of File
//
