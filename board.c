/*
 * board.c
 *
 *  Created on: 23 dic. 2020
 *      Author: carloscanodomingo
 */

//
// Included Files
//
#include <stdint.h>
#include "driverlib.h"
#include "scistdio.h"
#include "insert_name.h" //ToDo SRAS-45
#include "board.h"
//
// Function Prototypes
//
void PinMux_init(void);
void SCI_init(void);
void gpio_init(void);

extern void SCI_init(void);




void Board_init()
{
    EALLOW;

    PinMux_init();
    SCI_init();
    gpio_init();

    EDIS;
}

void PinMux_init()
{
    GPIO_setPinConfig(GPIO_24_MDXB);
    GPIO_setQualificationMode(24, GPIO_QUAL_ASYNC);

    GPIO_setPinConfig(GPIO_25_MDRB);
    GPIO_setQualificationMode(25, GPIO_QUAL_ASYNC);

    GPIO_setPinConfig(GPIO_26_MCLKXB);
    GPIO_setQualificationMode(26, GPIO_QUAL_ASYNC);

    GPIO_setPinConfig(GPIO_3_MCLKRB);
    GPIO_setQualificationMode(3, GPIO_QUAL_ASYNC);

    GPIO_setPinConfig(GPIO_27_MFSXB);
    GPIO_setQualificationMode(27, GPIO_QUAL_ASYNC);

    GPIO_setPinConfig(GPIO_1_MFSRB);
    GPIO_setQualificationMode(1, GPIO_QUAL_ASYNC);

    //ENABLE GENERAL PURPOSE INPUT OUTPUT PINS

    GPIO_setPinConfig(GPIO_6_GPIO6);        //enable GPIO_DEBUG_1

    GPIO_setPinConfig(GPIO_7_GPIO7);        //enable GPIO_DEBUG_2

    GPIO_setPinConfig(GPIO_8_GPIO8);        //enable GPIO_DEBUG_3

    GPIO_setPinConfig(GPIO_9_GPIO9);        //enable GPIO_DEBUG_4

    //SCIA -> mySCI0 Pinmux
    GPIO_setPinConfig(GPIO_18_SCITXDB);
    GPIO_setPinConfig(GPIO_19_SCIRXDB);

}

void gpio_init(){

    GPIO_setDirectionMode(GPIO_DEBUG_1, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(GPIO_DEBUG_1, GPIO_PIN_TYPE_STD);
    GPIO_setMasterCore(GPIO_DEBUG_1, GPIO_CORE_CPU1_CLA1);
    GPIO_setQualificationMode(GPIO_DEBUG_1, GPIO_QUAL_SYNC);

    GPIO_setDirectionMode(GPIO_DEBUG_2, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(GPIO_DEBUG_2, GPIO_PIN_TYPE_STD);
    GPIO_setMasterCore(GPIO_DEBUG_2, GPIO_CORE_CPU1_CLA1);
    GPIO_setQualificationMode(GPIO_DEBUG_2, GPIO_QUAL_SYNC);

    GPIO_setDirectionMode(GPIO_DEBUG_3, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(GPIO_DEBUG_3, GPIO_PIN_TYPE_STD);
    GPIO_setMasterCore(GPIO_DEBUG_3, GPIO_CORE_CPU1_CLA1);
    GPIO_setQualificationMode(GPIO_DEBUG_3, GPIO_QUAL_SYNC);

    GPIO_setDirectionMode(GPIO_DEBUG_4, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(GPIO_DEBUG_4, GPIO_PIN_TYPE_STD);
    GPIO_setMasterCore(GPIO_DEBUG_4, GPIO_CORE_CPU1_CLA1);
    GPIO_setQualificationMode(GPIO_DEBUG_4, GPIO_QUAL_SYNC);

}

void SCI_init(){

    SCIStdioConfig( LOG_SCI_BASE, LOG_SCI_BAUD, LOG_SCI_CLK);
}
