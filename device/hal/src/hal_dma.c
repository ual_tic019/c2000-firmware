/*
 * @file hal_dma.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 22 Jan 2021
 * @brief header file for implementing mcbsp functionality
 *
 *
 */

#include "hal_dma.h"
#include "bsp.h"
#include "dma.h"
#include "sysctl.h"
#include "hw_ints.h"

/**
 * Function to initalize DMA peripherals.
 *
 * @return error code.
 */

int hal_dma_init(void)
{
    //
    // Enable DMA peripheral.
    //
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_DMA);
    //
    // Configure DMA as secondary master for peripheral frame 2.
    //
    SysCtl_selectSecMaster(SYSCTL_SEC_MASTER_DMA, SYSCTL_SEC_MASTER_DMA);
    //
    // Initialize DMA.
    //
    DMA_initController();

    return HAL_DMA_NO_ERROR;
}

/**
 * Function to configure a specific DMA channel.
 *
 * @param[in] id - ID of the DMA channel to initialize.
 *
 * @param[in] isr_dma_handler - pointer to DMA interrupt service routine handler.
 *
 * @return error code.
 */

int hal_dma_config(uint16_t id, void (* isr_dma_handler)(void))
{

    //
    // Id check
    //
    if (id > DMA_TOTAL_NUM - 1)
        return HAL_DMA_INVALID_ID;
    //
    // Assign ISR
    //
    Interrupt_register(dma_channels[id].isr_addr, isr_dma_handler);
    DMA_disableInterrupt(dma_channels[id].channel_addr);
    //
    // Configure burst parameters.
    //
    DMA_configBurst(dma_channels[id].channel_addr,
                    dma_channels[id].dma_config_burst.burst_size,
                    dma_channels[id].dma_config_burst.src_burst_step,
                    dma_channels[id].dma_config_burst.dst_burst_step);

    //
    // Reset flags.
    //
    DMA_clearTriggerFlag(dma_channels[id].channel_addr);
    DMA_clearErrorFlag(dma_channels[id].channel_addr);

    //
    // Configure wrap parameters.
    //
    DMA_configWrap(dma_channels[id].channel_addr,
                   dma_channels[id].dma_config_wrap.src_wrap_size,
                   dma_channels[id].dma_config_wrap.src_wrap_step,
                   dma_channels[id].dma_config_wrap.dst_wrap_size,
                   dma_channels[id].dma_config_wrap.dst_wrap_step);

    //
    // Enable and configure interrupts
    //
    DMA_enableInterrupt(dma_channels[id].channel_addr);
    DMA_setInterruptMode(dma_channels[id].channel_addr, dma_channels[id].dma_interrupt_mode);
    DMA_enableTrigger(dma_channels[id].channel_addr);

    //
    // Set configuration mode.
    //
    DMA_configMode(dma_channels[id].channel_addr,
                   dma_channels[id].dma_trigger,
                   dma_channels[id].dma_config_mode);

    //
    // Spurious peripheral interrupts may have appeared, another clearing is needed.
    //
    DMA_clearTriggerFlag(dma_channels[id].channel_addr);

    return HAL_DMA_NO_ERROR;
}

/**
 * @brief hal_dma_deatch
 */

int hal_dma_detach(uint16_t id)
{
    return HAL_DMA_NO_ERROR;
}

/**
 * @brief hal_dma_attach
 */

int hal_dma_attach(uint16_t id)
{

    return HAL_DMA_NO_ERROR;
}

/**
 * Function to configure a DMA channel transfer parameters.
 *
 * @param[in] id - ID of the DMA channel to configure.
 *
 * @param[in] config_transfer - structure with configuration parameters.
 *
 * @return error code.
 */

int hal_dma_transfer_config(uint16_t id, hal_dma_config_transfer_t config_transfer)
{
    /* current implementation does not consider transfer configuration changes during
     * operation. This may require to stop interrupts and some other stuff! needs checking.
     */
    //
    // Id check
    //
    if (id > DMA_TOTAL_NUM - 1)
        return HAL_DMA_INVALID_ID;
    //
    // Transfer configuration
    //
    DMA_configTransfer(dma_channels[id].channel_addr,
                       config_transfer.transfer_size,
                       config_transfer.src_transfer_step,
                       config_transfer.dst_transfer_step);

    //
    // Address configuration
    //
    DMA_configAddresses(dma_channels[id].channel_addr,
                        config_transfer.dst_addr,
                        config_transfer.src_addr);

    return HAL_DMA_NO_ERROR;
}

/**
 * Function to make a specific DMA channel begin operation.
 *
 * @param[in] id - ID of the DMA channel to start.
 *
 * @return error code.
 */

int hal_dma_start(uint16_t id)
{
    //
    // Id check.
    //
    if (id > DMA_TOTAL_NUM)
        return HAL_DMA_INVALID_ID;

    DMA_startChannel(dma_channels[id].channel_addr);
    return HAL_DMA_NO_ERROR;
}

/**
 * Function to make a specific DMA channel stop its operations.
 *
 * @param[in] id - ID of the DMA channel to start.
 *
 * @return error code.
 */
int hal_dma_stop(uint16_t id)
{
    //
    // Id check
    //
    if (id > DMA_TOTAL_NUM)
        return HAL_DMA_INVALID_ID;

    DMA_stopChannel(dma_channels[id].channel_addr);
    return HAL_DMA_NO_ERROR;
}

/**
 * @brief hal_dma_is_attached
 */
int hal_dma_is_attached(uint16_t id)
{
    return HAL_DMA_NO_ERROR;
}

/**
 * Function to enable interrupt for a specific DMA channel.
 *
 * @param[in] id - ID of the DMA channel to start.
 *
 * @return error code.
 */
int hal_dma_enable_interrupt(uint16_t id)
{
    Interrupt_enable(dma_channels[id].isr_addr);
    //
    // All dma channels belong to CPU_INT7.
    //
    Interrupt_enableInCPU(INTERRUPT_CPU_INT7);
    return HAL_DMA_NO_ERROR;
}

/**
 * Function to disable interrupt for a specific DMA channel.
 *
 * @param[in] id - ID of the DMA channel to start.
 *
 * @return error code.
 */
int hal_dma_disable_interrupt(uint16_t id)
{
    Interrupt_disable(dma_channels[id].isr_addr);
    //
    // ToDo: deactivate pie group if no DMA channels are active.
    //
    return HAL_DMA_NO_ERROR;
}
