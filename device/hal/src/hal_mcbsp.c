/**
 * @file hal_mcbsp.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 22 Jan 2021
 * @brief header file for implementing mcbsp functionality
 *
 * Define MCBSP errors and functions
 */

/**
 * @brief Function definition of the peripheral
 */


#include    "hal_mcbsp.h"
#include    "bsp.h"


//
// Define to select clock delay.
//

#define MCBSP_CYCLE_NOP0(n)  __asm(" RPT #(" #n ") || NOP")
#define MCBSP_CYCLE_NOP(n)   MCBSP_CYCLE_NOP0(n)

/*
* Function to initialize the peripheral mcbsp
*
* @param[in] id - ID of the mcbsp to initialize
*
* @return error code
*/

int hal_mcbsp_init(uint16_t id)
{
	if (id > MCBSP_TOTAL_NUM - 1)
	{
		return HAL_MCBSP_INVALID_ID;
	}

	SysCtl_enablePeripheral(mcbsp_channels[id].channel_sysctl_clk);
	return HAL_MCBSP_NO_ERROR;
}

int hal_mcbsp_interrupt_enable(uint16_t id)
{
    if (id > MCBSP_TOTAL_NUM - 1)
    {
        return HAL_MCBSP_INVALID_ID;
    }

    if ( (mcbsp_channels[id].channel_direction == MCBSP_LOOP_MODE) ||
         (mcbsp_channels[id].channel_direction == MCBSP_DIRECTION_RX) )
    {
        McBSP_setRxInterruptSource(mcbsp_channels[id].channel_addr, (McBSP_RxInterruptSource) mcbsp_channels[id].interrupt_source);
        McBSP_enableRxInterrupt(mcbsp_channels[id].channel_addr);
    }

    if ( (mcbsp_channels[id].channel_direction == MCBSP_LOOP_MODE) ||
         (mcbsp_channels[id].channel_direction == MCBSP_DIRECTION_TX) )
    {
        McBSP_setTxInterruptSource(mcbsp_channels[id].channel_addr, (McBSP_TxInterruptSource) mcbsp_channels[id].interrupt_source);
        McBSP_enableTxInterrupt(mcbsp_channels[id].channel_addr);

    }


    //
    // Wait for CPU cycles equivalent to 2 SRG cycles-init delay.
    //
    MCBSP_CYCLE_NOP(8);

    return HAL_MCBSP_NO_ERROR;
}


/*
* Function to configure the peripheral mcbsp
*
* @param[in] id - ID of the mcbsp to initialize
*
* @return error code
*/
int hal_mcbsp_config(uint16_t id)
{

	if (id > MCBSP_TOTAL_NUM - 1)
	{
		return HAL_MCBSP_INVALID_ID;
	}

	//
	// Reset FS generator, sample rate generator, transmitter & receiver.
	//
	McBSP_resetFrameSyncLogic(mcbsp_channels[id].channel_addr);
	McBSP_resetSampleRateGenerator(mcbsp_channels[id].channel_addr);

	switch (mcbsp_channels[id].channel_direction)
	{

	case MCBSP_LOOP_MODE:
        McBSP_resetTransmitter(mcbsp_channels[id].channel_addr);
        McBSP_resetReceiver(mcbsp_channels[id].channel_addr);

        //
        // Set Rx sign-extension and justification mode.
        //
        McBSP_setRxSignExtension(mcbsp_channels[id].channel_addr,
                                 mcbsp_channels[id].rx_sign_extension_mode);

        //
        // Enable DLB mode. Comment out for non-DLB mode.
        //
        McBSP_enableLoopback(mcbsp_channels[id].channel_addr);

        // Set Rx & Tx delay.
        //
        McBSP_setRxDataDelayBits(mcbsp_channels[id].channel_addr,
                                 mcbsp_channels[id].data_delay_bits);
        McBSP_setTxDataDelayBits(mcbsp_channels[id].channel_addr,
                                 mcbsp_channels[id].data_delay_bits);

        //
        // Set CLKX & FSX source as sample rate generator.
        //
        McBSP_setTxClockSource(mcbsp_channels[id].channel_addr,
                               (McBSP_TxClockSource) mcbsp_channels[id].clk_src);


        McBSP_setTxFrameSyncSource(mcbsp_channels[id].channel_addr,
                                   (McBSP_TxFrameSyncSource) mcbsp_channels[id].framesync_source);

        //
        // Configure McBSP data behaviour.
        //
        McBSP_setRxDataSize(mcbsp_channels[id].channel_addr,
                            mcbsp_channels[id].mcbsp_config_data_size.data_frame,
                            mcbsp_channels[id].mcbsp_config_data_size.bits_per_word,
                            mcbsp_channels[id].mcbsp_config_data_size.words_per_frame);
        McBSP_setTxDataSize(mcbsp_channels[id].channel_addr,
                            mcbsp_channels[id].mcbsp_config_data_size.data_frame,
                            mcbsp_channels[id].mcbsp_config_data_size.bits_per_word,
                            mcbsp_channels[id].mcbsp_config_data_size.words_per_frame);
	    break;

	case MCBSP_DIRECTION_RX:
        McBSP_resetReceiver(mcbsp_channels[id].channel_addr);

        //
        // Set Rx sign-extension and justification mode.
        //
        McBSP_setRxSignExtension(mcbsp_channels[id].channel_addr,
                                 mcbsp_channels[id].rx_sign_extension_mode);

        //
        // Set Rx delay .
        //
        McBSP_setRxDataDelayBits(mcbsp_channels[id].channel_addr,
                                 mcbsp_channels[id].data_delay_bits);

        //
        // Set the source for CLKX & FSX as sample rate generator.
        //
        McBSP_setRxClockSource(mcbsp_channels[id].channel_addr,
                               (McBSP_RxClockSource) mcbsp_channels[id].clk_src);
        McBSP_setRxFrameSyncSource(mcbsp_channels[id].channel_addr,
                                   (McBSP_RxFrameSyncSource) mcbsp_channels[id].framesync_source);

        //
        // Configure McBSP data behaviour.
        //
        McBSP_setRxDataSize(mcbsp_channels[id].channel_addr,
                            mcbsp_channels[id].mcbsp_config_data_size.data_frame,
                            mcbsp_channels[id].mcbsp_config_data_size.bits_per_word,
                            mcbsp_channels[id].mcbsp_config_data_size.words_per_frame);

        //
        // Set LSPCLK as input source for sample rate generator.
        //
        const McBSP_SRGRxClockSource temp_srg_rx_clock_source =  (McBSP_SRGRxClockSource) mcbsp_channels[id].srg_clock_source;
        McBSP_setRxSRGClockSource(mcbsp_channels[id].channel_addr,
                                  temp_srg_rx_clock_source);

	    break;

	case MCBSP_DIRECTION_TX:
        McBSP_resetTransmitter(mcbsp_channels[id].channel_addr);

        //
        // Set Tx delay .
        //
        McBSP_setTxDataDelayBits(mcbsp_channels[id].channel_addr,
                                 mcbsp_channels[id].data_delay_bits);

        //
        // Set the source for CLKX & FSX as sample rate generator.
        //
        McBSP_setTxClockSource(mcbsp_channels[id].channel_addr,
                               (McBSP_TxClockSource) mcbsp_channels[id].clk_src);

        McBSP_setTxFrameSyncSource(mcbsp_channels[id].channel_addr,
                                   (McBSP_TxFrameSyncSource) mcbsp_channels[id].framesync_source);

        //
        // Configure McBSP data behaviour.
        //
        McBSP_setTxDataSize(mcbsp_channels[id].channel_addr,
                            mcbsp_channels[id].mcbsp_config_data_size.data_frame,
                            mcbsp_channels[id].mcbsp_config_data_size.bits_per_word,
                            mcbsp_channels[id].mcbsp_config_data_size.words_per_frame);
        //
        // Set LSPCLK as input source for sample rate generator.
        //
        const McBSP_SRGTxClockSource temp_srg_tx_clock_source =  (McBSP_SRGTxClockSource) mcbsp_channels[id].srg_clock_source;
        McBSP_setTxSRGClockSource(mcbsp_channels[id].channel_addr,
                                  temp_srg_tx_clock_source);

	    break;
	default:
        return HAL_MCBSP_INVALID_CFG;
	    break;
	}

    //
	// Set frame-sync pulse period.
	//
	McBSP_setFrameSyncPulsePeriod(mcbsp_channels[id].channel_addr,
	                              mcbsp_channels[id].frame_clock_divider);

	//
	// Set frame-sync pulse width.
	//
	McBSP_setFrameSyncPulseWidthDivider(mcbsp_channels[id].channel_addr,
	                                    mcbsp_channels[id].pulse_width_divider);

	if (mcbsp_channels[id].channel_direction == MCBSP_LOOP_MODE) {

	    //
	    // Set the trigger source for internally generated frame-sync pulse.
	    //
	    McBSP_setTxInternalFrameSyncSource(mcbsp_channels[id].channel_addr,
	                                       mcbsp_channels[id].internal_fs_source);


	    McBSP_setRxFrameSyncSource(mcbsp_channels[id].channel_addr, MCBSP_RX_EXTERNAL_FRAME_SYNC_SOURCE);

	    McBSP_setRxFrameSyncPolarity(mcbsp_channels[id].channel_addr, MCBSP_RX_FRAME_SYNC_POLARITY_LOW);

	    McBSP_setRxClockSource(mcbsp_channels[id].channel_addr, MCBSP_EXTERNAL_RX_CLOCK_SOURCE);

	    McBSP_setRxClockPolarity(mcbsp_channels[id].channel_addr, MCBSP_RX_POLARITY_FALLING_EDGE);


	    //
	    // Set LSPCLK as input source for sample rate generator.
	    //
	    const McBSP_SRGTxClockSource temp_srg_clock_source =  (McBSP_SRGTxClockSource) mcbsp_channels[id].srg_clock_source;
	    McBSP_setTxSRGClockSource(mcbsp_channels[id].channel_addr,
	                              temp_srg_clock_source);

	}
	//
	// Set Divide down value for CLKG.
	//
	McBSP_setSRGDataClockDivider(mcbsp_channels[id].channel_addr,
	                             mcbsp_channels[id].data_clock_divider);

	//
	// Set no external clock sync for CLKG.
	//
	McBSP_disableSRGSyncFSR(mcbsp_channels[id].channel_addr);

	return HAL_MCBSP_NO_ERROR;

}

/*
* Function to start the peripheral mcbsp
*
* @param[in] id - ID of the mcbsp to start
*
* @return error code
*/

int hal_mcbsp_start(uint16_t id)
{

	if (id > MCBSP_TOTAL_NUM - 1)
	{
		return HAL_MCBSP_INVALID_ID;
	}

	//
	// Enable Sample rate generator and wait for atleast 2 CLKG clock cycles.
	//
	McBSP_enableSampleRateGenerator(mcbsp_channels[id].channel_addr);

	//
	// Wait for CPU cycles equivalent to 2 CLKG cycles-init delay.
	// Total cycles required = 2*(SYSCLK/(LSPCLK/(1+CLKGDV_VAL))). In this
	// example LSPCLK = SYSCLK/4 and CLKGDV_VAL = 1.
	//
	MCBSP_CYCLE_NOP(16);

	//
	// Release Rx and/or Tx from reset.
	//
	if ( (mcbsp_channels[id].channel_direction == MCBSP_LOOP_MODE) ||
	   (mcbsp_channels[id].channel_direction == MCBSP_DIRECTION_RX) )
	{
        McBSP_enableReceiver(mcbsp_channels[id].channel_addr);
	}

    if ( (mcbsp_channels[id].channel_direction == MCBSP_LOOP_MODE) ||
       (mcbsp_channels[id].channel_direction == MCBSP_DIRECTION_TX) )
    {
	    McBSP_enableTransmitter(mcbsp_channels[id].channel_addr);
    }
    McBSP_enableFrameSyncLogic(mcbsp_channels[id].channel_addr);

	return HAL_MCBSP_NO_ERROR;
}

/*
* Function to stop the peripheral mcbsp
*
* @param[in] id - ID of the mcbsp to stop
*
* @return error code
*/
int hal_mcbsp_stop(uint16_t id)
{
	if (id > MCBSP_TOTAL_NUM - 1)
	{
		return HAL_MCBSP_INVALID_ID;
	}
	//
	// Reset FS generator, sample rate generator, transmitter & receiver.
	//
	McBSP_resetFrameSyncLogic(mcbsp_channels[id].channel_addr);
	McBSP_resetSampleRateGenerator(mcbsp_channels[id].channel_addr);

	if (mcbsp_channels[id].channel_direction == MCBSP_LOOP_MODE)
    {
        McBSP_enableReceiver(mcbsp_channels[id].channel_addr);
        McBSP_enableTransmitter(mcbsp_channels[id].channel_addr);
    }
    else
    {
        if (mcbsp_channels[id].channel_direction == MCBSP_DIRECTION_RX)
        {
            McBSP_resetTransmitter(mcbsp_channels[id].channel_addr);
        }
        else
        {
            McBSP_resetReceiver(mcbsp_channels[id].channel_addr);
        }
    }

	return HAL_MCBSP_NO_ERROR;
}

/*
* Function to write into mcbsp transmitter buffer
*
* @param[in] id - ID of the mcbsp to write to
* @param[in] data - data to be transmitted
*
* @return error code
*/

int hal_mcbsp_write(uint16_t id, uint32_t data)
{
    switch (mcbsp_channels[id].mcbsp_config_data_size.bits_per_word)
    {
        case MCBSP_BITS_PER_WORD_32:
        case MCBSP_BITS_PER_WORD_24:

            McBSP_write32bitData(mcbsp_channels[id].channel_addr, data);

            break;

        case MCBSP_BITS_PER_WORD_16:
        case MCBSP_BITS_PER_WORD_12:
        case MCBSP_BITS_PER_WORD_8:

            McBSP_write16bitData(mcbsp_channels[id].channel_addr, data);

            break;
    }

    return HAL_MCBSP_NO_ERROR;
}

/*
* Function to read from mcbsp receiver buffer
*
* @param[in] id - ID of the mcbsp to read from
* @param[in, out] data - variable to store read data
*
* @return error code
*/

int hal_mcbsp_read(uint16_t id, void (* data))
{
    switch (mcbsp_channels[id].mcbsp_config_data_size.bits_per_word)
    {
        case MCBSP_BITS_PER_WORD_32:
        case MCBSP_BITS_PER_WORD_24:
        {
            uint32_t* data_uint32 = (uint32_t*) data;
            *data_uint32 = McBSP_read32bitData(mcbsp_channels[id].channel_addr);

            break;
        }
        case MCBSP_BITS_PER_WORD_16:
        case MCBSP_BITS_PER_WORD_12:
        case MCBSP_BITS_PER_WORD_8:
        {
            uint16_t* data_uint16 = (uint16_t*) data;
            *data_uint16 = McBSP_read32bitData(mcbsp_channels[id].channel_addr);

            break;
        }
        default:
            return HAL_MCBSP_FAIL_READ;
    }

    return HAL_MCBSP_NO_ERROR;
}
