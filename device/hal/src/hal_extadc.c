/*
 * @file hal_dma.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 25 Jan 2021
 * @brief header file for implementing dma functionality
 *
 *
 */

#include "hal_extadc.h"
#include "hal_dma.h"
#include "hal_mcbsp.h"
#include "bsp.h"
#include "cla_shared.h"

/**
 * @brief extadc structures, variables and external functions
 */

#define WORD_SIZE_32            2
#define WORD_SIZE_16            1
#define DDR2_OFFSET             1
#define DMA_MODE_WORD_SIZE      0x4000U

hal_dma_config_transfer_t extadc_transfer_config =
{
	.transfer_size         = BUFFER_SIZE * EXT_ADC_NCHANNELS,
	.src_transfer_step     = DMA_BACKWARD_STEP,
	.dst_transfer_step     = EXTADC_DST_TRANSFER_STEP,
	.dst_addr              = NULL,
	.src_addr              = (const void*)(MCBSPB_BASE + MCBSP_O_DRR2),
};

/**
 * Function to initialize the peripherals required by the external ADC.
 *
 * @return error code.
 */

int hal_extadc_init(void)
{
#ifdef DMA_ENABLE
	if ( hal_dma_init() )
		return HAL_EXTADC_ERROR_DMA;
#endif

	if ( hal_mcbsp_init(MCBSP_EXT_ADC) )
		return HAL_EXTADC_ERROR_COM;

	return HAL_EXTADC_NO_ERROR;
}

/**
 * Function to initalize the peripherals required by the external ADC.
 *
 * @param[in] *isr_dma_handler pointer to DMA interrupt service routine handler.
 *
 * @param[in] config_transfer structure with DMA transfer configuration parameters.
 *
 * @return error code.
 */
int hal_extadc_config(void)
{
#ifdef EXTADC_DMA

        if ( hal_dma_config(DMA_EXT_ADC, isr_extadc_handler) )
            return HAL_EXTADC_ERROR_DMA;

        if ( hal_mcbsp_config(MCBSP_EXT_ADC, NULL, NULL) )
            return HAL_EXTADC_ERROR_COM;

#else

        if ( hal_mcbsp_config(MCBSP_EXT_ADC) )
            return HAL_EXTADC_ERROR_COM;

#endif
    return HAL_EXTADC_NO_ERROR;
}


/**
 * Function to initalize the peripherals required by the external ADC.
 *
 * @param[in] *ptr_buffer_receive pointer to buffer starting address.
 *
 * @param[in] buffer_size_words buffer size in words.
 *
 * @return error code.
 */

#ifdef EXTADC_DMA
int hal_extadc_transfer_config(uint16_t *ptr_buffer_receive, uint16_t buffer_size_words)
{
	uint16_t bytes_per_transfer;
#ifdef DMA_ENABLE
	if ( dma_channels[DMA_EXT_ADC].dma_config_mode & DMA_MODE_WORD_SIZE )
		bytes_per_transfer = WORD_SIZE_32 * dma_channels[DMA_EXT_ADC].dma_config_burst.burst_size *
		                     extadc_transfer_config.transfer_size;
	else
		bytes_per_transfer = WORD_SIZE_16  * dma_channels[DMA_EXT_ADC].dma_config_burst.burst_size *
		                     extadc_transfer_config.transfer_size;

	if ( buffer_size_words < bytes_per_transfer )
		return HAL_EXTADC_ERROR_BUFFER_TOO_SMALL;

	//
	// It is mandatory to read DDR2 before DDR1
	//
	extadc_transfer_config.dst_addr = ptr_buffer_receive + DDR2_OFFSET;

	if ( hal_dma_transfer_config(DMA_EXT_ADC, extadc_transfer_config) )
		return HAL_EXTADC_ERROR_DMA;
#endif
	return HAL_EXTADC_NO_ERROR;
}
#endif

int hal_extadc_start(void)
{

#ifdef EXTADC_DMA
    if( hal_dma_start(DMA_EXT_ADC) )
        return HAL_EXTADC_ERROR_DMA;

    if( hal_dma_enable_interrupt(DMA_EXT_ADC) )
        return HAL_EXTADC_ERROR_DMA;

#else
    if ( hal_mcbsp_interrupt_enable(MCBSP_EXT_ADC) )
        return HAL_EXTADC_ERROR_COM;
#endif
    if ( hal_mcbsp_start(MCBSP_EXT_ADC) )
		return HAL_EXTADC_ERROR_COM;

	return HAL_EXTADC_NO_ERROR;
}

int hal_extadc_stop(void)
{
#ifdef EXTADC_DMA
    if ( hal_dma_stop(DMA_EXT_ADC) )
        return HAL_EXTADC_ERROR_DMA;
#endif

    if ( hal_mcbsp_stop(MCBSP_EXT_ADC) )
		return HAL_EXTADC_ERROR_COM;

	return HAL_EXTADC_NO_ERROR;
}

#ifndef EXTADC_DMA
int hal_extadc_receive(void (* data))
{
    if ( hal_mcbsp_read(MCBSP_EXT_ADC, data) )
        return HAL_EXTADC_ERROR_COM;

    return HAL_EXTADC_NO_ERROR;
}

int hal_extadc_dummy_send( uint32_t data )
{
    if ( hal_mcbsp_write(MCBSP_EXT_ADC, data) )
        return HAL_EXTADC_ERROR_COM;

    return HAL_EXTADC_NO_ERROR;
}
#endif
