/**
 * bsp.c
 *
 *  Created by Manuel Soler Ortiz [msolerortiz@ual.es]
 *  & Carlos Cano Domingo [carcandom@ual.es]
 *
 *  TIC-019 UAL
 */


#include "bsp.h"


const mcbsp_config_t mcbsp_channels[MCBSP_TOTAL_NUM] =
{
#ifndef DEBUG
#ifdef MCBSP_1
 {
  .channel_addr                 =   MCBSPB_BASE,
  .channel_sysctl_clk           =   SYSCTL_PERIPH_CLK_MCBSPB,
  .channel_direction            =   MCBSP_DIRECTION_RX,

  .rx_sign_extension_mode       =   MCBSP_RIGHT_JUSTIFY_FILL_SIGN,          ///< Set Rx sign-extension and justification mode.
  .data_delay_bits              =   MCBSP_DATA_DELAY_BIT_1,                 ///< Set Rx & Tx delay to 1 cycle.
  .clk_src                      =   (uint16_t) MCBSP_INTERNAL_RX_CLOCK_SOURCE,         ///< Set the source for CLKX as sample rate generator.
  .framesync_source             =   (uint16_t) MCBSP_RX_INTERNAL_FRAME_SYNC_SOURCE,    ///< Set the source for FSX as sample rate generator.
  .interrupt_source             =   (uint16_t) MCBSP_RX_ISR_SOURCE_FRAME_SYNC,
  .mcbsp_config_data_size       =   {                                       ///< Configure McBSP data behaviour RX
                                     .data_frame = MCBSP_PHASE_ONE_FRAME,
                                     .bits_per_word = MCBSP_BITS_PER_WORD_24,
                                     .words_per_frame = EXT_ADC_NCHANNELS - 1,
  },

  .frame_clock_divider          =   EXT_ADC_FSP,
  .pulse_width_divider          =   0U,                                             ///< Set frame-sync pulse width.-> 0 + 1
  .srg_clock_source             =   (uint16_t) MCBSP_SRG_RX_CLOCK_SOURCE_LSPCLK,    ///< Set LSPCLK as input source for sample rate generator.
  .internal_fs_source           =   MCBSP_TX_INTERNAL_FRAME_SYNC_SRG,               ///< Set the trigger source for internally generated frame-sync pulse.
  .data_clock_divider           =   2U,                                             ///< Set Divide down value for CLKG -> 2 + 1
 }
#endif
#else
#ifdef MCBSP_1
 {
  .channel_addr                 =   MCBSPB_BASE,
  .channel_sysctl_clk           =   SYSCTL_PERIPH_CLK_MCBSPB,
  .channel_direction            =   MCBSP_LOOP_MODE,

  .rx_sign_extension_mode       =   MCBSP_RIGHT_JUSTIFY_FILL_SIGN,                     ///< Set Rx sign-extension and justification mode.
  .data_delay_bits              =   MCBSP_DATA_DELAY_BIT_1,                            ///< Set Rx & Tx delay to 1 cycle.
  .clk_src                      =   (uint16_t) MCBSP_INTERNAL_TX_CLOCK_SOURCE,         ///< Set the source for CLKX as sample rate generator.
  .framesync_source             =   (uint16_t) MCBSP_TX_INTERNAL_FRAME_SYNC_SOURCE,    ///< Set the source for FSX as sample rate generator.
  .interrupt_source             =   (uint16_t) MCBSP_TX_ISR_SOURCE_FRAME_SYNC,
  .mcbsp_config_data_size       =   {                                                  ///< Configure McBSP data behaviour RX
                                     .data_frame = MCBSP_PHASE_ONE_FRAME,
                                     .bits_per_word = MCBSP_BITS_PER_WORD_24,
                                     .words_per_frame = 3,
  },

  .frame_clock_divider          =   EXT_ADC_FSP,
  .pulse_width_divider          =   0U,                                             ///< Set frame-sync pulse width.-> 0 + 1
  .srg_clock_source             =   (uint16_t) MCBSP_SRG_TX_CLOCK_SOURCE_LSPCLK,    ///< Set LSPCLK as input source for sample rate generator.
  .internal_fs_source           =   MCBSP_TX_INTERNAL_FRAME_SYNC_SRG,              ///< Only used in loop mode; initialized as zero.
  .data_clock_divider           =   2U,                                             ///< Set Divide down value for CLKG -> 2 + 1
 }
#endif
#endif

};

const dma_config_t dma_channels[DMA_TOTAL_NUM] =
{
#ifdef DMA_1
 {
  .channel_addr      = DMA_CH1_BASE,                            ///< High priority DMA channel
  .isr_addr          = INT_DMA_CH1,                             ///<
  .dma_config_burst  =  {
                        .burst_size     =  2U,                  ///<
                        .src_burst_step =  1U,                  ///< Read fist RDR2 and then RDR1
                        .dst_burst_step =  DMA_BACKWARD_STEP,   ///< Come back to RDR2
  },
  .dma_config_wrap   = {
                        .src_wrap_size = DMA_MAX_VALUE,         ///< no wrap on Source.
                        .src_wrap_step = 0U,
                        .dst_wrap_size = EXTADC_DST_WRAP_SIZE,
                        .dst_wrap_step = EXTADC_DST_WRAP_STEP,
  },
  .dma_interrupt_mode = DMA_INT_AT_BEGINNING,
  .dma_trigger        = DMA_TRIGGER_MCBSPBMREVT,
  .dma_config_mode    = DMA_CFG_CONTINUOUS_ENABLE | DMA_CFG_SIZE_16BIT,
 }
#endif
#ifdef DMA_2
 {
  .channel_addr      = DMA_CH1_BASE,                            ///< High priority DMA channel
  .dma_config_burst  =  {
                        .burst_size     =  2U,                  ///<
                        .src_burst_step =  1U,                  ///< Read fist RDR2 and then RDR1
                        .dst_burst_step =  DMA_BACKWARD_STEP,   ///< Come back to RDR2
  },
  .dma_config_wrap   = {
                        .src_wrap_size = 0x10000U,
                        .src_wrap_step = 0U,
                        .dst_wrap_size = 4U,
                        .dst_wrap_step = 2U,
  },
  .dma_interrupt_mode = DMA_INT_AT_BEGINNING,
  .dma_trigger        = DMA_TRIGGER_MCBSPBMREVT,
  .dma_config_mode    = DMA_CFG_CONTINUOUS_ENABLE | DMA_CFG_SIZE_16BIT,
 }
#endif
};




