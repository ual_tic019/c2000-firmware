/**
 * @file bsp.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 22 Jan 2021
 * @brief
 *
 * Define ons
 */


#ifndef BSP_H_
#define BSP_H_

#include "sysctl.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "dma.h"
#include "mcbsp.h"
#include <stdint.h>



//
// Peripheral configuration defines
//
#define DMA_TOTAL_NUM               (1)
#define DMA_BACKWARD_STEP           0xFFFF
#define DMA_MAX_VALUE               0x10000U
#define DMA_1

#define MCBSP_TOTAL_NUM             (1)
#define MCBSP_1

//
// ADS1274 defines
//
#define EXT_ADC_NCHANNELS           4       ///< Number of channel of the external ADC (ADS1274 -> 4 channels)
#define EXT_ADC_FSP                 256     ///< Frame Sync Period High Speed Mode -> 256
#define EXTADC_DST_TRANSFER_STEP    201U
#define EXTADC_DST_WRAP_SIZE        4U
#define EXTADC_DST_WRAP_STEP        2U




//////////////////////////////////////////////////////////
///////////
///////////         DMA CONFIG
///////////
//////////////////////////////////////////////////////
typedef struct
{
    uint16_t                        burst_size;             ///<
    int16_t                         src_burst_step;         ///<
    int16_t                         dst_burst_step;         ///<
} dma_config_burst_t;

typedef struct
{
    uint32_t                        src_wrap_size;          ///<
    int16_t                         src_wrap_step;          ///<
    uint32_t                        dst_wrap_size;          ///<
    int16_t                         dst_wrap_step;          ///<
} dma_config_wrap_t;

//////////////////////////////////////////////////////////
///////////
///////////         MCBSP CONFIG
///////////
//////////////////////////////////////////////////////
typedef struct
{
    McBSP_DataPhaseFrame            data_frame;
    const McBSP_DataBitsPerWord     bits_per_word;
    uint16_t                        words_per_frame;

}mcbsp_config_data_size_t;

typedef enum
{
    MCBSP_DIRECTION_RX,
    MCBSP_DIRECTION_TX,
    MCBSP_LOOP_MODE
}mcbsp_channel_direction_t;

typedef struct
{
    uint32_t                        channel_addr;           ///<
    uint32_t                        isr_addr;               ///<
    dma_config_burst_t              dma_config_burst;       ///<
    dma_config_wrap_t               dma_config_wrap;        ///<
    DMA_InterruptMode               dma_interrupt_mode;     ///<
    DMA_Trigger                     dma_trigger;            ///<
    uint32_t                        dma_config_mode;        ///<
} dma_config_t;

typedef struct
{
    uint32_t                        channel_addr;
    uint32_t                        pie_rx_addr;
    uint32_t                        pie_tx_addr;
    SysCtl_PeripheralPCLOCKCR       channel_sysctl_clk;
    mcbsp_channel_direction_t       channel_direction;
    McBSP_RxSignExtensionMode       rx_sign_extension_mode;
    McBSP_DataDelayBits             data_delay_bits;
    uint16_t                        clk_src;
    uint16_t                        framesync_source;
    uint16_t                        interrupt_source;
    mcbsp_config_data_size_t        mcbsp_config_data_size;
    uint16_t                        frame_clock_divider;
    uint16_t                        pulse_width_divider;
    uint16_t                        srg_clock_source;
    McBSP_TxInternalFrameSyncSource internal_fs_source;
    uint16_t                        data_clock_divider;
} mcbsp_config_t;

typedef enum {
    DMA_EXT_ADC,

} dma_channel_id;

extern const dma_config_t dma_channels[DMA_TOTAL_NUM];


typedef enum {
#ifdef MCBSP_1
    MCBSP_EXT_ADC,
#endif
} mcbsp_channel_id;

extern const mcbsp_config_t mcbsp_channels[MCBSP_TOTAL_NUM];
#endif /* BSP_H_ */
