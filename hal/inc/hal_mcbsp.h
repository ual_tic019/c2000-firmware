/**
 * @file hal_mcbsp.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 22 Jan 2021
 * @brief header file for implementing mcbsp functionality
 *
 * Define MCBSP errors and functions
 */
#ifndef HAL_MCBSP_H_
#define HAL_MCBSP_H_

#include <stdint.h>
#include "mcbsp.h"

/**
 * @brief Specific errors of the peripheral
 */


#define HAL_MCBSP_NO_ERROR      (0)
#define HAL_MCBSP_INVALID_ID    (-1)
#define HAL_MCBSP_FAIL_READ     (-2)
#define HAL_MCBSP_INVALID_CFG   (-3)
#define HAL_MCBSP_NO_TX_ISR     (-4)
/**
 * @brief Function definition of the peripheral
 */

int hal_mcbsp_init              (uint16_t id);

int hal_mcbsp_config            (uint16_t id);
int hal_mcbsp_start             (uint16_t id);
int hal_mcbsp_stop              (uint16_t id);
int hal_mcbsp_interrupt_enable  (uint16_t id);
int hal_mcbsp_read              (uint16_t id, void (* data));
int hal_mcbsp_write             (uint16_t id, uint32_t data);

#endif /* HAL_INC_HAL_MCBSP_H_ */
