/*
 * @file hal_dma.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 25 Jan 2021
 * @brief header file for implementing dma functionality
 *
 *
 */

#ifndef HAL_INC_HAL_DMA_H_
#define HAL_INC_HAL_DMA_H_

#include <stdint.h>

/**
 * @brief Specific errors of the peripheral
 */
#define HAL_DMA_NO_ERROR            (0)
#define HAL_DMA_INVALID_ID          (-1)

/**
 * @brief dma structures, variables and external functions
 */

typedef struct
{
	uint32_t                    transfer_size;              ///< Number of transfer per DMA operation
	int16_t                     src_transfer_step;          ///< Steps by transfer in source buffer.
	int16_t                     dst_transfer_step;          ///< Steps by transfer in destination buffer.
	const void                  *dst_addr;                  ///< Destination buffer address.
	const void                  *src_addr;                  ///< Source buffer address.
} hal_dma_config_transfer_t;

/**
 * @brief function prototypes
 */

int hal_dma_init                (void);
int hal_dma_config              (uint16_t id, void (*isr_dma_handler)(void));
int hal_dma_detach              (uint16_t id);
int hal_dma_attach              (uint16_t id);
int hal_dma_transfer_config     (uint16_t id, hal_dma_config_transfer_t config_transfer);
int hal_dma_start               (uint16_t id);
int hal_dma_stop                (uint16_t id);
int hal_dma_is_attached         (uint16_t id);
int hal_dma_enable_interrupt    (uint16_t id);
int hal_dma_disable_interrupt   (uint16_t id);

#endif /* HAL_INC_HAL_DMA_H_ */
