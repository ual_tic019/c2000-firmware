/*
 * @file hal_cla.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 23 Mar 2021
 * @brief header file for implementing dma functionality
 *
 *
 */


#ifndef HAL_INC_HAL_CLA_H_
#define HAL_INC_HAL_CLA_H_

/**
 * @brief Specific errors of the peripheral
 */
#define HAL_CLA_NO_ERROR            (0)



extern __interrupt void cla1Isr1 ();

int hal_init_cla(void);

void hal_cla_config_memory(void);

void hal_cla_task_config(void);

#endif /* HAL_INC_HAL_CLA_H_ */
