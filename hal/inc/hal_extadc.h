/*
 * @file hal_extadc.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 28 Jan 2021
 * @brief header file for implementing external ADC functionality
 *
 *
 */

#ifndef HAL_INC_HAL_EXTADC_H_
#define HAL_INC_HAL_EXTADC_H_

#include <stdint.h>
#include "hw_types.h"

/**
 * @brief Specific errors
 */
#define HAL_EXTADC_NO_ERROR                  (0)
#define HAL_EXTADC_ERROR_COM                 (-1)
#define HAL_EXTADC_ERROR_DMA                 (-2)
#define HAL_EXTADC_ERROR_BUFFER_TOO_SMALL    (-3)

/**
 * @brief function prototypes
 */

int hal_extadc_init(void);
int hal_extadc_config(void);
int hal_extadc_start(void);
int hal_extadc_stop(void);

# ifdef EXTADC_DMA
int hal_extadc_transfer_config(uint16_t *ptr_buffer_receive, uint16_t buffer_size_words);

#else
int hal_extadc_receive(void *data);
int hal_extadc_dummy_send( uint32_t data );

# endif

#endif /* HAL_INC_HAL_EXTADC_H_ */
