/*
 * @file hal_spi.h
 * @author Manuel Soler Ortiz & Carlos Cano Domingo
 * @date 01 Feb 2021
 * @brief header file for implementing external SPI functionality
 *
 *
 */


#ifndef HAL_INC_HAL_SPI_H_
#define HAL_INC_HAL_SPI_H_


#include <stdint.h>
#include "hw_types.h"

/**
 * @brief Specific errors
 */
#define HAL_SPI_NO_ERROR                   (0)
#define HAL_SPI_ERROR_COM                 (-1)
#define HAL_SPI_ERROR_DMA                 (-2)
#define HAL_SPI_ERROR_BUFFER_TOO_SMALL    (-3)
/**
 * @brief function prototypes
 */

int hal_spi_init(uint32_t id);
int hal_spi_reset(uint32_t id);
int hal_spi_is_busy(uint32_t id);
int hal_spi_config(uint32_t id);
int hal_spi_send_flush(uint32_t id);
int hal_spi_write_data(uint32_t id, uint16_t * buffer_write, uint16_t size, uint16_t data_write, uint16_t timeout_ms);
int hal_spi_read_data(uint32_t id, uint16_t * buffer_read, uint16_t size, uint16_t data_write, uint16_t timeout_ms);

#endif /* HAL_INC_HAL_EXTADC_H_ */

#endif /* HAL_INC_HAL_SPI_H_ */
