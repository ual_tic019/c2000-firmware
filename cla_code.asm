;;#############################################################################
;; \file \cs30_f2837x\F2837xD_examples_Cpu1\cla_adc_fir32\cpu01\fir32.asm
;;
;; \brief  5-Tap FIR Filter Example
;; \date   September 26, 2013
;;
;;
;; Group: 			C2000
;; Target Family:	F2837x
;;
;;(C)Copyright 2013, Texas Instruments, Inc.
;;#############################################################################
;;$TI Release: F2837xD Support Library v3.11.00.00 $
;;$Release Date: Sun Oct  4 15:55:24 IST 2020 $
;;$Copyright:
;// Copyright (C) 2013-2020 Texas Instruments Incorporated - http://www.ti.com/
;//
;// Redistribution and use in source and binary forms, with or without
;// modification, are permitted provided that the following conditions
;// are met:
;//
;//   Redistributions of source code must retain the above copyright
;//   notice, this list of conditions and the following disclaimer.
;//
;//   Redistributions in binary form must reproduce the above copyright
;//   notice, this list of conditions and the following disclaimer in the
;//   documentation and/or other materials provided with the
;//   distribution.
;//
;//   Neither the name of Texas Instruments Incorporated nor the names of
;//   its contributors may be used to endorse or promote products derived
;//   from this software without specific prior written permission.
;//
;// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;// $
;;#############################################################################

;;*****************************************************************************
;; includes
;;*****************************************************************************
	.cdecls C, LIST, "cla_shared.h"
;;*****************************************************************************
;; defines
;;*****************************************************************************

_DDR2 .set 0x00006040U				;
_DDR1 .set 0x00006041U
_RRDY .set 0x00006045U
		;

	;.define 30, TOTAL_SAMPLES 		;
	.define 0x00000CE0, CLA_INT_REG
	;.define 0x00000CE2, CLA_INT_FRC_REG
	;.define 0x00001423, CLA_MICLR
	.define 0x00007F00, GPIO_DATA		;GPIO_06, GPIO_07, GPIO_08, GPIO_09
	.define #cla_sh_tmp + 6, _last_y; 0x5u
	.define 0x00000002, mask_rrdy
	;.define #(0x8002 + (2 * 4 * MAX_SAMPLES_CLA)).0, _ptr_last
; 2 (float32) * (4 (N_channels) * FIR LEN - 1)

_XA0 .set cla_sh_val
_XA1 .set cla_sh_val + 2 * FIR_LEN
_XA2 .set cla_sh_val + 4 * FIR_LEN
_XA3 .set cla_sh_val + 6 * FIR_LEN


X_EOR .set cla_sh_val + (2 * ( 4 * FIR_LEN - 1))
; 2 (float32) * (4 (N_channels) * FIR LEN - 1)

A_EOR .set cla_sh_coef + (2 * ( 4 * FIR_LEN - 1))
; 2 (float32) * (4 (N_channels) * FIR LEN - 1)

LOOP_ITER .set FIR_LEN - 2

BUFFER_LENGTH .set WORDS_PER_BUFFER

;_OUT_EOR .set  cla_sh_out + (2 * EXT_ADC_NCHANNELS * MAX_SAMPLES_CLA)

;;*****************************************************************************
;; function definitions
;;*****************************************************************************
;// CLA code must be within its own assembly section and must be
;// even aligned.  Note: since all CLA instructions are 32-bit
;// this alignment naturally occurs and the .align 2 is most likely
;// redundant

       .sect        "Cla1Prog"
_Cla1Prog_Start
       .align       2

Cla1Task1:
	;START_GPIO_DEBUG --- SET ALL GPIOS
	;MMOVXI		MR1, #0x03C0	; write 1s to bits 6, 7, 8, and 9.
	;MNOP
	;MNOP
	;MNOP
	;MMOV16		@GPIO_DATA, MR1	; set GPIO06, GPIO07, GPIO08, GPIO09
	;END_GPIO_DEBUG

	MMOVXI 		MR1, #0x0001 	; SAVING-TIME: prepare value for CLA_INT_REG
	MMOVXI 		MR3, _last_y	; Cpy fix addr to reset the position of the first copy to the intermeditate buffer
	MMOVI32		MR0, mask_rrdy
	MNOP
	MMOV16 		@CLA_INT_REG, MR1
	MMOV16 		@cla_sh_ptr_tmp, MR3	; Copy fix addr to ptr var

Repat_sample_0:
	MMOVZ16 	MR3, @_RRDY
	MAND32 		MR2, MR3, MR0 ;  MR0 & MR2 ==> if(MR2 == 0) ZF == 1
	MNOP
	MNOP
	MNOP
	MBCNDD Repat_sample_0, EQ
	MNOP
	MNOP
	MNOP

	MMOVZ16 	MR3, @_DDR2 		; Load (31:16) Bits of McBsP
	MLSL32		MR3, #16		; Move 16 bits to the left
	MMOVZ16		MR2, @_DDR1 		; Load (15:0) Bits of McBsP
	MADD32		MR1, MR3, MR2	; Add low part with upper part of the MCBSP READ
	MI32TOF32 	MR3, MR1		; Convert to float32
	MMOV32 		@_XA0, MR3		; save in XA0

Repat_sample_1:
	MMOVZ16 	MR3, @_RRDY
	MAND32 		MR2, MR3, MR0 ;  MR0 & MR2 ==> if(MR2 == 0) ZF == 1
	MNOP
	MNOP
	MNOP
	MBCNDD Repat_sample_1, EQ
	MNOP
	MNOP
	MNOP

	MMOVZ16 	MR3, @_DDR2 		; Load (31:16) Bits of McBsP
	MLSL32		MR3, #16		; Move 16 bits to the left
	MMOVZ16		MR2, @_DDR1 		; Load (15:0) Bits of McBsP
	MADD32		MR1, MR3, MR2	; Add low part with upper part of the MCBSP READ
	MI32TOF32 	MR3, MR1		; Convert to float32

	MMOV32 		@_XA1, MR3		; save in XA1

Repat_sample_2:
	MMOVZ16 	MR3, @_RRDY
	MAND32 		MR2, MR3, MR0 ;  MR3 & MR2 ==> if(MR2 == 0) ZF == 1
	MNOP
	MNOP
	MNOP
	MBCNDD Repat_sample_2, EQ
	MNOP
	MNOP
	MNOP

	MMOVZ16 	MR3, @_DDR2 	; Load (31:16) Bits of McBsP
	MLSL32		MR3, #16		; Move 16 bits to the left
	MMOVZ16		MR2, @_DDR1 	; Load (15:0) Bits of McBsP
	MADD32		MR1, MR3, MR2	; Add low part with upper part of the MCBSP READ
	MI32TOF32 	MR3, MR1		; Convert to float32

	MMOV32 		@_XA2, MR3		; save in XA2

Repat_sample_3:
	MMOVZ16 	MR3, @_RRDY
	MAND32 		MR2, MR3, MR0 ;  MR3 & MR2 ==> if(MR2 == 0) ZF == 1
	MNOP
	MNOP
	MNOP
	MBCNDD Repat_sample_3, EQ
	MNOP
	MNOP
	MNOP


	MMOVZ16 	MR3, @_DDR2 	; Load (31:16) Bits of McBsP
	MLSL32		MR3, #16		; Move 16 bits to the left
	MMOVZ16		MR2, @_DDR1 	; Load (15:0) Bits of McBsP
	MADD32		MR1, MR3, MR2	; Add low part with upper part of the MCBSP READ
	MI32TOF32 	MR3, MR1		; Convert to float32

	MMOV32 		@_XA3, MR3		; save in XA3

	MMOVI16 MAR0, #X_EOR		; load X end of register address
	MMOVI16 MAR1, #A_EOR		; load A end of register address
	MNOP
	MNOP

	;START_GPIO_DEBUG --- UNSET_GPIO_DEBUG_4
	;MMOVXI		MR1, #0x01C0	; write 1s to bits 6, 7 and 8.
	;MNOP
	;MNOP
	;MNOP
	;MMOV16		@GPIO_DATA, MR1	; set GPIO06, GPIO07 and GPIO08, unset GPIO09
	;END_GPIO_DEBUG

	.loop EXT_ADC_NCHANNELS
		MMOV32 		MR0, *MAR0[-2]++
		MMOV32 		MR1, *MAR1[-2]++
		MMPYF32		MR2, MR0, MR1
	||  MMOV32 MR1, *MAR1[-2]++

		MMOV32 	MR0, *MAR0[-2]++
		MMOV32  *MAR0+[+4], MR0
		MMOVIZ	MR3, #0.0

		.loop  LOOP_ITER
			MMACF32 MR3, MR2, MR2, MR0, MR1
		 || MMOV32 MR1, *MAR1[-2]++
			MMOV32 MR0, *MAR0[-2]++
			MMOV32 *MAR0+[+4], MR0

		.endloop

		MMOV16 @cla_sh_temp_mar1, MAR1; Saving ptr to A
		MMOV16 MAR1, @cla_sh_ptr_tmp		; Reduce MNOP cycles

		MMACF32 MR3, MR2, MR2, MR0, MR1
		|| MMOV32 MR1, @cla_sh_ptr_tmp ; Doing Nothing
		MMOV32 *MAR0+[+4], MR0


		MADDF32 MR3, MR3, MR2

		MMOV32 *MAR1[-2]++, MR3

		MMOV16     	@cla_sh_ptr_tmp, MAR1               ; Output
		MMOV16		MAR1, @cla_sh_temp_mar1;
		MNOP
		MNOP
		MNOP

	.endloop


	; START_GPIO_DEBUG --- UNSET_GPIO_DEBUG_3
	;MMOVXI		MR1, #0x00C0	; write 1s to bits 6 and 7.
	;MNOP
	;MNOP
	;MNOP
	;MMOV16		@GPIO_DATA, MR1	; set GPIO06 and GPIO07, unset GPIO08 and GPIO09
	; END_GPIO_DEBUG

	MMOVIZ			MR2, #0
	MMOVXI			MR2, #1
	MMOVZ16	 		MR0, @cla_sh_cnt_sample
	MMOVZ16			MR1, @cla_sh_ds_rate
	MADD32			MR0, MR0, MR2
	MCMP32 			MR0, MR1
	MNOP
	MNOP
	MNOP

	MBCNDD Skip, NEQ
	MNOP
	MNOP
	MNOP

	MNOP
	MNOP
	MNOP
	MDEBUGSTOP
	MNOP
	MNOP
	MNOP

		MMOVXI 		MR0, #0x0000
		MMOV16 		MAR0, @cla_sh_ptr_out	;Load addr of the output register
		MNOP
		MNOP
		MMOV32 		MR1, @cla_sh_tmp 		;Load the 1st output of the fir register
		MF32TOI32 	MR2, MR1				; Convert to int32 (TO CPU)
		MMOV32 		*MAR0[+2]++, MR2			; Move to the output addr

		MMOV32 		MR1, @cla_sh_tmp + 2		; Load the 2nd output of the fir register
		MF32TOI32 	MR2, MR1				; Convert to int32 (TO CPU)
		MMOV32 		*MAR0[+2]++, MR2			; Move to the output addr

		MMOV32 		MR1, @cla_sh_tmp + 4		; Load the 3rd output of the fir register
		MF32TOI32 	MR2, MR1				; Convert to int32 (TO CPU)
		MMOV32 		*MAR0[+2]++, MR2			; Move to the output addr

		MMOV32 		MR1, @cla_sh_tmp + 6		; Load the 4th output of the fir register
		MF32TOI32 	MR2, MR1				; Convert to int32 (TO CPU)
		MMOV32 		*MAR0[+2]++, MR2			; Move to the output addr

		MMOV16 		@cla_sh_ptr_out, MAR0	; Save the changed ptr of the output register
		MMOVIZ		MR1, #0					; Clean MR1 for address load.
		MMOVIZ		MR3, #0					; Clean MR3 for address load.
		MMOVZ16 	MR2, @cla_sh_ptr_out

		;MI16TOF32 MR1, @last_position	; Compare with the last position of the output buffer (CLAtoCPU1)

		MMOVZ16 	MR1, @cla_sh_next_buf_addr
		MMOVZ16 	MR3, @cla_sh_eob_addr

	; START_GPIO_DEBUG --- UNSET GPIO_DEBUG_2
	;MMOVXI		MR3, #0x0040	; write 1s to bits 6.
	;MNOP
	;MNOP
	;MNOP
	;MMOV16		@GPIO_DATA, MR3	; set GPIO06, unset GPIO07, GPIO08 and GPIO09
	; END_GPIO_DEBUG
		MNOP
		MNOP
		MNOP
		MCMP32 		MR1, MR2
		MNOP

		MNOP
		MNOP
		MBCNDD 		Skip, GT
		MNOP
		MNOP
		MNOP

		MCMP32 MR3, MR2
		MMOVXI MR1, #cla_sh_out
		MNOP
		MNOP
		MBCNDD Next_buffer, GT
		MNOP
		MNOP
		MNOP

		MMOV16 @cla_sh_next_buf_addr, MR1					; Structure wrap; back to 1st buffer

Next_buffer:

			MMOVIZ		MR1, #0
			MMOVXI		MR1, #1
			MMOV32 		MR3, @cla_sh_free_cnt 				; free counter used for package numbering
			MADD32		MR2, MR3, MR1
			MMOV16	 	MAR0, @cla_sh_next_buf_addr				; end of buffer reached, ready to write next buffer.
			MMOVXI 		MR1, #0x0000						; to clear CLA_INT_REG FLAGS
			MNOP
			MMOV32		@cla_sh_free_cnt, MR2				; value stored, kept on MR3 for header writing.
			MMOV32 		*MAR0[+2]++, MR2					; Write pkg_number
			MMOV16 		@CLA_INT_REG, MR1					; CLA_INT_REG = 0
			MMOV16 		@cla_sh_ptr_out, MAR0				; cla_sh_ptr_out = next buffer, ready for next iteration. Offset to produce next buffer address.
			MNOP
			MMOVIZ		MR2, #0.0
			MMOVXI		MR2, #BUFFER_LENGTH
			MMOVZ16		MR1, @cla_sh_ptr_out
			MADD32		MR3, MR2, MR1
			MNOP
			MNOP
			MNOP
			MMOV16		@cla_sh_next_buf_addr, MR3
			; START_GPIO_DEBUG --- UNSET GPIO_DEBUG_1
			;MMOVXI		MR1, #0x0000	; write 0s to bits 6.
			;MNOP
			;MNOP
			;MNOP
			;MMOV16		@GPIO_DATA, MR1	; unset GPIO06, GPIO07, GPIO08 and GPIO09
			; END_GPIO_DEBUG


Skip:

	MNOP
    MNOP
    MNOP
	MMOV16     @cla_sh_cnt_sample, MR0               ; Output

	MMOVI32		MR0, mask_rrdy
    MNOP
    MNOP

flush:
	MMOVZ16 	MR3, @_RRDY
	MAND32 		MR2, MR3, MR0 ;  MR0 & MR2 ==> if(MR2 == 0) ZF == 1
	MNOP
	MNOP
	MNOP

	MBCNDD end_flush, EQ

	MNOP
	MNOP
	MNOP

	MMOVZ16 	MR3, @_DDR1 		; Load (31:16) Bits of McBsP
	MBCNDD 		flush, UNC
	MNOP
	MNOP
	MNOP


end_flush:

	MNOP
	MNOP
	MNOP
	MSTOP
    MNOP
    MNOP
    MNOP

Cla1T1End:


Cla1Task2:
    MSTOP
    MNOP
    MNOP
    MNOP
Cla1T2End:

Cla1Task3:
    MSTOP
    MNOP
    MNOP
    MNOP
Cla1T3End:


Cla1Task4:
    MSTOP
    MNOP
    MNOP
    MNOP
Cla1T4End:


Cla1Task5:
    MSTOP
    MNOP
    MNOP
    MNOP
Cla1T5End:

Cla1Task6:
    MSTOP
    MNOP
    MNOP
    MNOP
Cla1T6End:

Cla1Task7:
    MSTOP
    MNOP
    MNOP
    MNOP                     ; End task
Cla1T7End:

Cla1Task8:
    MSTOP
    MNOP
    MNOP
    MNOP
Cla1T8End:
Cla1Prog_End:
	.end
;; End of file
