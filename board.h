/*
 * board.h
 *
 *  Created on: 23 dic. 2020
 *      Author: carloscanodomingo
 */

#ifndef BOARD_H_
#define BOARD_H_


#define LOG_SCI_BASE SCIB_BASE
#define LOG_SCI_BAUD 115200
#define LOG_SCI_CLK DEVICE_LSPCLK_FREQ


void    Board_init();
void    SCI_init();
void    PinMux_init();

#define GPIO_DEBUG_1    6
#define GPIO_DEBUG_2    7
#define GPIO_DEBUG_3    8
#define GPIO_DEBUG_4    9

#endif /* BOARD_H_ */
