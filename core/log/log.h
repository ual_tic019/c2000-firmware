/*
 * debug.h
 *
 *  Created on: 22 ene. 2021
 *      Author: carloscanodomingo
 */

#ifndef CORE_LOG_LOG_H_
#define CORE_LOG_LOG_H_

#include "scistdio.h"

// log levels

#define LEVEL_DEBUG     0
#define LEVEL_INFO      1
#define LEVEL_WARN      2
#define LEVEL_ERROR     3
#define LEVEL_NONE      4

#define LOG_PRINT(message, ...) SCIprintf(message "\033[0\n\r", ## __VA_ARGS__);

#define DEBUG_LEVEL (LEVEL_DEBUG)


#define CYAN_BG "\033[1;30;102m"

#define LOG_DEBUG(message, ...)          LOG(LEVEL_DEBUG,  message, ## __VA_ARGS__)
#define LOG_INFO(message, ...)           LOG(LEVEL_INFO, message, ## __VA_ARGS__)
#define LOG_WARN(message, ...)           LOG(LEVEL_WARN, message, ## __VA_ARGS__)
#define LOG_ERROR(message, ...)          LOG(LEVEL_ERROR, message, ## __VA_ARGS__)

const char * lvl_text_debug[] = {"DEBUG", "INFO", "WARN", "ERROR"};
const char * lvl_color_debug[] = {"\033[1;37;102m", "\033[1;232;104m", "\033[1;37;43m", "\033[1;37;41m"};

#define LOG(level, message, ...) \
   if (DEBUG_LEVEL <= level) { \
      LOG_PRINT("%s%s" "\033[1;96;49m" "\t-- %s -- \t" "\033[1;39;49m "  message, lvl_color_debug[level],lvl_text_debug[level], __func__ , ## __VA_ARGS__);  \
   } \


#endif /* CORE_LOG_LOG_H_ */

/*
 *
 * "\033[{FORMAT_ATTRIBUTE};{FORGROUND_COLOR};{BACKGROUND_COLOR}m{TEXT}\033[{RESET_FORMATE_ATTRIBUTE}m"
 *
 * FORMAT
 *
 * { "Default", "0" },
{ "Bold", "1" },
{ "Dim", "2" },
{ "Underlined", "3" },
{ "Blink", "5" },
{ "Reverse", "7" },
{ "Hidden", "8" }
 *
 * FOREGROUND COLOR
 *
 * { "Default", "39" },
{ "Black", "30" },
{ "Red", "31" },
{ "Green", "32" },
{ "Yellow", "33" },
{ "Blue", "34" },
{ "Magenta", "35" },
{ "Cyan", "36" },
{ "Light Gray", "37" },
{ "Dark Gray", "90" },
{ "Light Red", "91" },
{ "Light Green", "92" },
{ "Light Yellow", "93" },
{ "Light Blue", "94" },
{ "Light Magenta", "95" },
{ "Light Cyan", "96" },
{ "White", "97" }




 * BACKGROUND COLOR
 *
  { "Default", "49" },
{ "Black", "40" },
{ "Red", "41" },
{ "Green", "42" },
{ "Yellow", "43" },
{ "Blue", "44" },
{ "Megenta", "45" },
{ "Cyan", "46" },
{ "Light Gray", "47" },
{ "Dark Gray", "100" },
{ "Light Red", "101" },
{ "Light Green", "102" },
{ "Light Yellow", "103" },
{ "Light Blue", "104" },
{ "Light Magenta", "105" },
{ "Light Cyan", "106" },
{ "White", "107" }
 */
