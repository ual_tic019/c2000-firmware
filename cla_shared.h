/*
 * cla_shared.h
 *
 *  Created on: 10 feb. 2021
 *      Author: carloscanodomingo
 */

#ifndef CLA_SHARED_H_
#define CLA_SHARED_H_

//
// Included Files
//

//#include "F2837xD_Cla_defines.h"
#include <stdint.h>
#include "bsp.h"
//
// Defines
//

#define EXT_ADC_NCHANNELS           4
#define BUFFER_SIZE                 200
#define MAX_BUFFERS                 5
#define FIR_LEN                     13
#define WORDS_PER_BUFFER            2 * EXT_ADC_NCHANNELS * BUFFER_SIZE

typedef uint32_t buffer_cla_channel_t[BUFFER_SIZE];
typedef float data_channel_t[FIR_LEN];
typedef int32_t data_cla_slice_t[EXT_ADC_NCHANNELS];
typedef struct
{
    uint32_t                        pkg_number;
    data_cla_slice_t                buffer[BUFFER_SIZE];
}data_cla_out_t;

//
//  Globals
//

//
//  Task 1 (C) Variables
//
/*
    #pragma DATA_SECTION(X,"CLADataLS0")
     data_sample_t X[FIR_LEN];
    //#pragma DATA_SECTION(Y,"CLADataLS0")
    extern uint16_t Y;
    #pragma DATA_SECTION(cla_sh_ptr_tmp,"CLADataLS0")
    float * cla_sh_ptr_tmp;
*/


extern data_channel_t   cla_sh_coef[EXT_ADC_NCHANNELS];
extern data_channel_t   cla_sh_val[EXT_ADC_NCHANNELS];
extern float32_t        cla_sh_tmp[EXT_ADC_NCHANNELS];
extern uint16_t         cla_sh_cnt_sample;
extern float32_t*       cla_sh_ptr_tmp;
extern int32_t*         cla_sh_ptr_out;
extern uint16_t         cla_sh_temp_mar1;
extern data_cla_out_t   cla_sh_out[MAX_BUFFERS];
extern uint32_t         cla_sh_free_cnt;
extern uint16_t         cla_sh_next_buf_addr;
extern uint16_t         cla_sh_eob_addr;
extern uint16_t         cla_sh_ds_rate;
//
//  Task 2 (C) Variables
//

//
//  Task 3 (C) Variables
//

//
//  Task 4 (C) Variables
//

//
//  Task 5 (C) Variables
//

//
//  Task 6 (C) Variables
//

//
//  Task 7 (C) Variables
//

//
//  Task 8 (C) Variables
//

//
//  Common (C) Variables
//


//
// Function Prototypes
//
// The following are symbols defined in the CLA assembly code
// Including them in the shared header file makes them
// .global and the main CPU can make use of them.
//
__interrupt void Cla1Task1();
__interrupt void Cla1Task2();
__interrupt void Cla1Task3();
__interrupt void Cla1Task4();
__interrupt void Cla1Task5();
__interrupt void Cla1Task6();
__interrupt void Cla1Task7();
__interrupt void Cla1Task8();



//
// End of file
//



#endif /* CLA_SHARED_H_ */
